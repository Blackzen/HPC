#pragma OPENCL EXTENSION cl_amd_byte_addressable_store : enable
__kernel void vectorsum(__global int *a, __global int *b, __global int *c)
{
	int id = get_global_id(0);
		c[id] = a[id] + b[id];
	
}