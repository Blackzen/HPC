#include <CL/cl.h>
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>


#define SUCCESS 0
#define FAILURE 1

using namespace std;

cl_device_id* printDeviceInfo()
	{
		int i, j;
		char* value;
		size_t valueSize;
		cl_uint platformCount;
		cl_platform_id* platforms;
		cl_uint deviceCount;
		cl_device_id* devices;
		cl_uint maxComputeUnits;
		cl_ulong MEM_SIZE;

		// get all platforms
		clGetPlatformIDs(0, NULL, &platformCount);
		platforms = (cl_platform_id*)malloc(sizeof(cl_platform_id) * platformCount);
		clGetPlatformIDs(platformCount, platforms, NULL);

		for (i = 0; i < platformCount; i++) {

			// get all devices
			clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 0, NULL, &deviceCount);
			devices = (cl_device_id*)malloc(sizeof(cl_device_id) * deviceCount);
			clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, deviceCount, devices, NULL);

			// for each device print critical attributes
			for (j = 0; j < deviceCount; j++) {

				// print device name
				cout << devices[j] << endl;
				clGetDeviceInfo(devices[j], CL_DEVICE_NAME, 0, NULL, &valueSize);
				value = (char*)malloc(valueSize);
				clGetDeviceInfo(devices[j], CL_DEVICE_NAME, valueSize, value, NULL);
				printf("%d. Device: %s\n", j + 1, value);
				free(value);

				// print hardware device version
				clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, 0, NULL, &valueSize);
				value = (char*)malloc(valueSize);
				clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, valueSize, value, NULL);
				printf(" %d.%d Hardware version: %s\n", j + 1, 1, value);
				free(value);

				// print software driver version
				clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, 0, NULL, &valueSize);
				value = (char*)malloc(valueSize);
				clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, valueSize, value, NULL);
				printf(" %d.%d Software version: %s\n", j + 1, 2, value);
				free(value);

				// print c version supported by compiler for device
				clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, 0, NULL, &valueSize);
				value = (char*)malloc(valueSize);
				clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, valueSize, value, NULL);
				printf(" %d.%d OpenCL C version: %s\n", j + 1, 3, value);
				free(value);

				// print parallel compute units
				clGetDeviceInfo(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS,
					sizeof(maxComputeUnits), &maxComputeUnits, NULL);
				printf(" %d.%d Parallel compute units: %d\n", j + 1, 4, maxComputeUnits);


				/*//print max_mem_alloc_size
				clGetDeviceInfo(devices[j], CL_DEVICE_MAX_MEM_ALLOC_SIZE,
					sizeof(MEM_SIZE), &MEM_SIZE, NULL);*/

				//printf("Memory size: %d\n", MEM_SIZE);
			}
			return devices;
			free(devices);
		}

		free(platforms);

	}


void showAditionalinfo(){
	cl_device_id* devices;
	char* value;
	size_t valueSize;
	cl_ulong MEM_SIZE;
	cl_ulong GLOBAL_MEM_SIZE;
	cl_ulong GLOBAL_MEM_CACHE_SIZE;
	cl_ulong LOCAL_MEM_SIZE;
	size_t *workGroupSize;

	cl_uint workGroupDimentions;
	size_t Itemsizes;
	cl_uint maxComputeUnits;


	///1.DEVASTADOR
	///2.OLAND
	///3.AMD A10-5750 APU with Radeon<tm > 

	devices = printDeviceInfo();

	int device_index = 0;


	while (1){

		cout << endl << endl << "choose number of device you want to see specs." << endl << endl;
		cin >> device_index;
		cout << "USED DEVICE." << endl;

		clGetDeviceInfo(devices[device_index], CL_DEVICE_NAME, 0, NULL, &valueSize);
		value = (char*)malloc(valueSize);
		clGetDeviceInfo(devices[device_index], CL_DEVICE_NAME, valueSize, value, NULL);
		printf("Device: %s\n", value);

		// print parallel compute units
		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_COMPUTE_UNITS,
			sizeof(maxComputeUnits), &maxComputeUnits, NULL);
		printf("Parallel compute units: %d\n", maxComputeUnits);

		//print max_mem_alloc_size
		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_MEM_ALLOC_SIZE,
			sizeof(MEM_SIZE), &MEM_SIZE, NULL);

		printf("Max Memory size: %d\n", MEM_SIZE);

		//print max_goblal_chache_alloc_size
		clGetDeviceInfo(devices[device_index], CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
			sizeof(GLOBAL_MEM_SIZE), &GLOBAL_MEM_SIZE, NULL);

		printf("Max global Memory size: %d\n", GLOBAL_MEM_SIZE);

		//print max_goblal_chacheLine_alloc_size
		clGetDeviceInfo(devices[device_index], CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
			sizeof(GLOBAL_MEM_CACHE_SIZE), &GLOBAL_MEM_CACHE_SIZE, NULL);

		printf("Max global Memory cache line  size: %d\n", GLOBAL_MEM_CACHE_SIZE);

		//prIT LOCAL MEM SIZE.
		clGetDeviceInfo(devices[device_index], CL_DEVICE_LOCAL_MEM_SIZE,
			sizeof(LOCAL_MEM_SIZE), &LOCAL_MEM_SIZE, NULL);

		printf("Max local  size: %d\n", LOCAL_MEM_SIZE);

		/*	//prIT	workgroup size.
		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_WORK_GROUP_SIZE,
		sizeof(workGroupSize), &workGroupSize, NULL);

		printf("workgroup size: %s\n", workGroupSize);*/

		/*
		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
		sizeof(workGroupDimentions), &workGroupDimentions, NULL);

		printf("workgroup dimentions: %d\n", workGroupDimentions);

		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_WORK_ITEM_SIZES,
		sizeof(Itemsizes), &Itemsizes, NULL);

		printf("work items sizes per dimention: %s\n", Itemsizes);
		*/

		/*free(value);
		string s;
		cin >> s;*/

	}

}



/* convert the kernel file into a string */
int convertToString(const char *filename, std::string& s)
{
	size_t size;
	char*  str;
	std::fstream f(filename, (std::fstream::in | std::fstream::binary));

	if (f.is_open())
	{
		size_t fileSize;
		f.seekg(0, std::fstream::end);
		size = fileSize = (size_t)f.tellg();
		f.seekg(0, std::fstream::beg);
		str = new char[size + 1];
		if (!str)
		{
			f.close();
			return 0;
		}

		f.read(str, fileSize);
		f.close();
		str[size] = '\0';
		s = str;
		delete[] str;
		return 0;
	}
	cout << "Error: failed to open file\n:" << filename << endl;
	return FAILURE;
}



cl_platform_id getPlatform()
	{
		/*Step1: Getting platforms and choose an available one.*/
		cl_uint numPlatforms;	//the NO. of platforms
		cl_platform_id platform = NULL;	//the chosen platform
		cl_int	status = clGetPlatformIDs(0, NULL, &numPlatforms);
		if (status != CL_SUCCESS)
		{
			cout << "Error: Getting platforms!" << endl;
			return 0;
		}

		cout <<"NUMBER OF PLATFORMS: "<< endl << numPlatforms << endl;

		/*For clarity, choose the first available platform. */
		if (numPlatforms > 0)
		{
			cl_platform_id* platforms = (cl_platform_id*)malloc(numPlatforms* sizeof(cl_platform_id));
			status = clGetPlatformIDs(numPlatforms, platforms, NULL);
			platform = platforms[0];
			/*cout << "CHOOSED PLATFORM;" << endl;
			cout << platform << endl << endl;*/
			free(platforms);
		}
		return platform;
}


cl_device_id* getDevice(cl_platform_id platform)
	{
		/*Step 2:Query the platform and choose the first GPU device if has one.Otherwise use the CPU as device.*/
		cl_int	status;
		cl_uint				numDevices = 0;
		cl_device_id        *devices;
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &numDevices);
		if (numDevices == 0)	//no GPU available.
		{
			cout << "No GPU device available." << endl;
			cout << "Choose CPU as default device." << endl;
			status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 0, NULL, &numDevices);
			devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
			status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, numDevices, devices, NULL);
		}
		else
		{
			devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
			status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numDevices, devices, NULL);
		}
		/*cout << "CHOOSED DEVICE." << endl;
		cout << devices << endl<<endl;*/

		return devices;
}

cl_program getCLProgram(const char *kernelName, cl_context context)
	{
		cl_int	status;
		/*Step 5: Create program object */
		const char *filename = kernelName;
		string sourceStr;
		status = convertToString(filename, sourceStr);
		const char *source = sourceStr.c_str();
		size_t sourceSize[] = { strlen(source) };
		cl_program program = clCreateProgramWithSource(context, 1, &source, sourceSize, NULL);
	
		return program;
}



////////////PROGRAMS TO RUN  MAKE SURE YOU PASS IN THE ARGUMENTS AND SET THIS FUNTION EN PROGRAM INPUT



void HOLA_MUNDO(cl_context context, cl_program program, cl_command_queue& commandQueue)
	{
		cl_int	status;
		////HOLA MUNDO. /////
		const char* input = "GdkknVnqkc";
		size_t strlength = strlen(input);
		cout << "input string:" << endl;
		cout << input << endl;
		char *output = (char*)malloc(strlength + 1);

		//CREATE NEEDED BUFFERS.
		cl_mem inputBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (strlength + 1) * sizeof(char), (void *)input, NULL);
		cl_mem outputBuffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, (strlength + 1) * sizeof(char), NULL, NULL);

		/*Step 8: Create kernel object */
		cl_kernel kernel = clCreateKernel(program, "test", NULL);

		/*Step 9: Sets Kernel arguments.*/
		status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&inputBuffer);
		status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&outputBuffer);


		/*Step 10: Running the kernel.*/
		size_t global_work_size[1] = { strlength };
		status = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, global_work_size, NULL, 0, NULL, NULL);

		/*Step 11: Read the cout put back to host memory.*/
		status = clEnqueueReadBuffer(commandQueue, outputBuffer, CL_TRUE, 0, strlength * sizeof(char), output, 0, NULL, NULL);

		output[strlength] = '\0';	//Add the terminal character to the end of output.
		cout << "\noutput string:" << endl;
		cout << output << endl;



		//buen ciudadano y liberamos recursos
		status = clReleaseKernel(kernel);//Release the program object.
		status = clReleaseMemObject(inputBuffer);		//Release mem object.
		status = clReleaseMemObject(outputBuffer);
		if (output != NULL)
		{
			free(output);
			output = NULL;
		}

		cout << "HELLO WORLD Passed!\n";


	}


void TEST(cl_context context, cl_program program, cl_command_queue& commandQueue)
{
	cl_int	status;
	////HOLA MUNDO. /////
	const char* input = "GdkknVnqkc";
	size_t strlength = strlen(input);
	cout << "input string:" << endl;
	cout << input << endl;
	char *output = (char*)malloc(strlength + 1);

	//CREATE NEEDED BUFFERS.
	cl_mem inputBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (strlength + 1) * sizeof(char), (void *)input, NULL);
	cl_mem outputBuffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, (strlength + 1) * sizeof(char), NULL, NULL);

	/*Step 8: Create kernel object */
	cl_kernel kernel = clCreateKernel(program, "test", NULL);

	/*Step 9: Sets Kernel arguments.*/
	status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&inputBuffer);
	status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&outputBuffer);


	/*Step 10: Running the kernel.*/
	size_t global_work_size[1] = { strlength };
	status = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, global_work_size, NULL, 0, NULL, NULL);

	/*Step 11: Read the cout put back to host memory.*/
	status = clEnqueueReadBuffer(commandQueue, outputBuffer, CL_TRUE, 0, strlength * sizeof(char), output, 0, NULL, NULL);

	output[strlength] = '\0';	//Add the terminal character to the end of output.
	cout << "\noutput string:" << endl;
	cout << output << endl;



	//buen ciudadano y liberamos recursos
	status = clReleaseKernel(kernel);//Release the program object.
	status = clReleaseMemObject(inputBuffer);		//Release mem object.
	status = clReleaseMemObject(outputBuffer);
	if (output != NULL)
	{
		free(output);
		output = NULL;
	}

	cout << "HELLO WORLD Passed!\n";


}


void VECTOR_SUM(cl_context context, cl_program program, cl_command_queue& commandQueue)
{
	cl_int	status;
	////VECTOR SUM /////
	
	// Length of vectors
	int n = 128;

	// Host input vectors
	cl_double *h_a;
	cl_double *h_b;
	// Host output vector
	cl_double *h_c;

	// Size, in bytes, of each vector
	size_t bytes = n*sizeof(cl_double);



	// Allocate memory for each vector on host
	h_a = (cl_double*)malloc(bytes);
	h_b = (cl_double*)malloc(bytes);
	h_c = (cl_double*)malloc(bytes);

	// Initialize vectors on host
	int i;
	for (i = 0; i < n; i++)
	{
		h_a[i] = i;
		h_b[i] = n-i;
		cout<<"h_a	"<< h_a[i] << endl;
		cout << "h_b	" << h_b[i] << endl;
	}


	// Device input buffers
	cl_mem d_a = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, bytes, (void *)h_a, NULL);

	cl_mem d_b = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, bytes, (void *)h_b, NULL);

	// Device output buffer
	cl_mem d_c = clCreateBuffer(context, CL_MEM_WRITE_ONLY, bytes, NULL, NULL);

	cl_int err;

	/*Step 8: Create kernel object */
	cl_kernel kernel = clCreateKernel(program, "vectorsum", &err);

	cout << "1	" << err << endl;

	/*Step 9: Sets Kernel arguments.*/
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)d_a);
	cout << "2	" << err << endl;
	err = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)d_b);
	cout << "3	" << err << endl;
	err = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)d_c);
	cout << "4	" << err << endl;

	//err = clSetKernelArg(kernel, 3, sizeof(int), (int*)n);
	//cout << "5	" << err << endl;
	
	// Number of work items in each local work group
	size_t localSize = 32;

	// Number of total work items - localSize must be devisor
	size_t globalSize = ceil(n / (cl_double)localSize)*localSize;
	
	cout << "global size :	" << globalSize << endl;
	cout << "local size :	" << localSize << endl;
	/*Step 10: Running the kernel.*/
	// Execute the kernel over the entire range of the data set  
	err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, &globalSize, &localSize,
		0, NULL, NULL);

	cout << "6	" << err << endl;

	/*Step 11: Read the cout put back to host memory.*/
	clEnqueueReadBuffer(commandQueue, d_c, CL_TRUE, 0, bytes, h_c, 0, NULL, NULL);

	// Wait for the command queue to get serviced before reading back results
	clFinish(commandQueue);


	//Sum up vector c and print result divided by n, this should equal 1 within error
	cl_double sum = 0;
	for (i = 0; i < n; i++)
		{
			sum += h_c[i];
			cout << h_c[i] << endl;
	}
		
	printf("final result: %f\n", sum / n);


	//buen ciudadano y liberamos recursos

	clReleaseMemObject(d_a);
	clReleaseMemObject(d_b);
	clReleaseMemObject(d_c);
	clReleaseKernel(kernel);	
	cout << "VECTOR SUM Passed!\n";


}



int main(int argc, char* argv[]) {
	
	//KEEP CONTROL OF OPERATION IF IT FAILS.
	cl_int	status;
	//showAditionalinfo();
	
	/*Step 1: Get platform.*/
	cl_platform_id platform = NULL;
	platform = getPlatform();

	/*Step 1: Get Device.*/
	cl_device_id* device;
	device = getDevice(platform);

	/*Step 3: Create context.*/
	cl_context context = clCreateContext(NULL, 1, device, NULL, NULL, NULL);

	/*Step 4: Creating command queue associate with the context.*/
	cl_command_queue commandQueue = clCreateCommandQueue(context, device[0], 0, NULL);




	
	
	///////////////ииииииииииииentradas del programaииииииииииииии//////////////////

	//cout << "INICIO HOLA MUNDO" << endl << endl;
	/*Step 5: Get program.*/
	cl_program program;
	//program = getCLProgram("New_Kernel.cl", context);

	/*Step 6: Build program. */
	//clBuildProgram(program, 1, device, NULL, NULL, NULL);

	/*Step 7: INITIALIZE PROBLEM.*/
	//HOLA_MUNDO(cl_context context, cl_program program, cl_command_queue& commandQueue)
	
	//HOLA_MUNDO(context, program, commandQueue);

	
/*///иииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии
//иииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии
//иииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии*/

	cout << "INICIO SUMA DE VECTORES" << endl << endl;
	/*Step 5: Get program.*/
	program = getCLProgram("vector_sum.cl", context);

	/*Step 6: Build program. */
	clBuildProgram(program, 1, device, NULL, NULL, NULL);

	/*Step 7: INITIALIZE PROBLEM.*/

	VECTOR_SUM(context, program, commandQueue);


/*///иииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии
//иииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии
//иииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии*/


	
	//RESERVADO PARA MULT DE MATRICES.	//RESERVADO PARA MULT DE MATRICES CON TECNICA DE TILES.
		//-tener presente la jerarquia de memorias de ope cd y diferecia con cuda.





	//buen ciudadano y liberamos recursos

	/*Step 12: Clean the resources.*/
				//Release kernel.
	status = clReleaseProgram(program);
	status = clReleaseCommandQueue(commandQueue);	//Release  Command queue.
	status = clReleaseContext(context);

	if (device != NULL)
	{
		free(device);
		device = NULL;
	}



	cout << "EXECUTION DONE.";
	string s;
	cin >> s;

	return 0;
}