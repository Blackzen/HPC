#include <time.h>
#include <stdio.h>

void randomMemInit(float* data, int size)
{
	int i;

	for (i = 0; i < size; ++i)
		data[i] = rand();
}

long timediff(clock_t t1, clock_t t2) {
    long elapsed;
    elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
    return elapsed;
}


void matrizMult(float *A , float *B,float *C,int WA, int HA,int WB ,int HB)
    {
        int progress = 0;
        int index_A = 0;
        int index_B = 0;
        int tam = HA*WB;
        printf("TAM : %d\n\n", tam);
        printf("size %d %d\n", HA,WB);
        int i = 0;
        int j = 0;
        float result = 0.0;

        int temp1 = 0;
        int temp2 = 0;
        index_B = 0;
        int k =0;

        for (i = 0; i< HA ; i++)
            {

            for(k =0 ; k< WB ;k++){

                for (j=0; j<HB; j++)
                    {
                        index_A = (i*WA)+(j);
                        temp1 = A[index_A];
                        //printf("%d*",temp1);


                        index_B = k+(j*WB);
                        temp2 = B[index_B];
                        //printf("%d + ",temp2);

                        result += temp1*temp2;
                    }
                 //i = i + HA;
                    //printf(" = %f \n",result);
                    C[progress] = result;
                    progress++;
                    result = 0;
            }

        }
    }


int main()
    {
        clock_t t1, t2;
        long elapsed;


        t1 = clock();


        int WA = 32;
        //scanf("igrese el # de filas de A ;%d",WA);

        int HA = 32;
        //scanf("igrese el # de columnas de A ;%d",HA);

        int WB = 32;

        int HB = 32;

        //scanf("igrese el # de filas de B ;%d",WB);

        //scanf("igrese el # de columnas de B ;%d",HB);

        int WC = HA;
        int HC = WB;

        // set seed for rand()
        srand(10);

	//Allocate host memory for matrices A and B
	unsigned int size_A = WA * HA;
	unsigned int mem_size_A = sizeof(float) * size_A;
	float* h_A = (float*)malloc(mem_size_A);

	unsigned int size_B = WB * HB;
	unsigned int mem_size_B = sizeof(float) * size_B;
	float* h_B = (float*)malloc(mem_size_B);


	//Allocate host memory for the result C
	unsigned int size_C = WC * HC;
	unsigned int mem_size_C = sizeof(float) * size_C;
	float* h_C = (float*)malloc(mem_size_C);


	//Initialize host memory
	randomMemInit(h_A, size_A);
	randomMemInit(h_B, size_B);
    randomMemInit(h_C, size_C);
/*
    	printf("\n\nMatrix A\n");
    	printf("size: [%d,%d]\n",WA,HA);
	int i;
	for (i = 0; i < size_A; i++)
	{
		printf("%f ", h_A[i]);
		if (((i + 1) % WA) == 0)
			printf("\n");
	}
	printf("\n");

printf("\n\nMatrix B \n");
printf("size: [%d,%d]\n",WB,HB);
	int j;
	for (j = 0; j < size_B; j++){

		printf("%f ", h_B[j]);
		if (((j + 1) % WB) == 0)
			printf("\n");
	}
	printf("\n");

*/

    if(WA == HB)
        {
            matrizMult(h_A ,h_B ,h_C ,WA ,HA ,WB ,HB );
                //print out the results

  /*          printf("\n\nMatrix C (Results)\n");
            printf("size: [%d,%d]\n",WC,HC);
            int k;
            for (k = 0; k < size_C; k++)
            {
                printf("%f ", h_C[k]);
                if (((k + 1) % WC) == 0)
                    printf("\n");
            }
            printf("\n");

*/
            printf("Matrix multiplication completed...\n");

        }
    else
        {
            printf("PAILA MATRICES DE DIFERENTE TAMANO. ");
            printf("[%d,%d] X ",WA,HA);
            printf("[%d,%d]\n\n",WB,HB);
            printf("%d != %d\n",WA,HB);
            if(HA == WB)
                {
                    printf("\n********************************************************");
                    printf("\n*SI QUIERE TRANSPONGA LAS MATRICES A LO MEJOR FUNCIONA.*");
                    printf("\n********************************************************\n\n\n\n\n");
                }
        }

    t2 = clock();

    elapsed = timediff(t1, t2);

    printf("elapsed: %ld ms\n", elapsed);

return 0;
 }

