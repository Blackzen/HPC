
#ifdef cl_khr_fp64
    #pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
    #pragma OPENCL EXTENSION cl_amd_fp64 : enable
#else
    #error "Double precision floating point not supported by OpenCL implementation."
#endif


__kernel void test(__global unsigned char* in, __global unsigned char* out, __constant int* x_filter, __constant int* y_filter,__private const  int width, __private const  int height)
{
	int i = get_global_id(0);

	float sum = 0.0;
	float sum2 = 0.0;

	if(i>width-1 && i<(width*height) -width)
		{

			int p1= in[i-1-width];
			int p2= in[i-width];
			int p3= in[i+1-width];
			int p4= in[i-1];
			int p5= in[i];
			int p6= in[i+1];
			int p7= in[i-1+width];
			int p8= in[i+width];
			int p9= in[i+1+width];

			if(i%width != 0 && i%width != width-1 )
				{

					sum += p1*x_filter[0];
					sum += p2*x_filter[1];
					sum += p3*x_filter[2];
					sum += p4*x_filter[3];
					sum += p5*x_filter[4];
					sum += p6*x_filter[5];
					sum += p7*x_filter[6];
					sum += p8*x_filter[7];
					sum += p9*x_filter[8];

					//out[i] = sum;
					
					//sobely
					sum2 += p1*y_filter[0];
					sum2 += p2*y_filter[1];
					sum2 += p3*y_filter[2];
					sum2 += p4*y_filter[3];
					sum2 += p5*y_filter[4];
					sum2 += p6*y_filter[5];
					sum2 += p7*y_filter[6];
					sum2 += p8*y_filter[7];
					sum2 += p9*y_filter[8];
				

					//sobel 2d
					out[i] = sqrt(sum*sum + sum2*sum2);
					
				}
			

			
		}
	
	

}
