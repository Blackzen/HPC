#include <opencv\cv.h>
#include <opencv\highgui.h>
#include <vector>
#include <CL\cl.h>


#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SUCCESS 0
#define FAILURE 1

using namespace cv;
using namespace std;

void  pixels_to_filfer(Mat& image, Mat& Result, int i, int* filter, int* filter2)
{

	int pixel_index = i;

	int offset = image.size().width;

	int val0 = image.data[(pixel_index - 1)-offset];
	int val1 = image.data[pixel_index - offset];
	int val2 = image.data[(pixel_index + 1)-offset];

	int val3 = image.data[pixel_index - 1];
	int val4 = image.data[pixel_index];
	int val5 = image.data[pixel_index + 1];

	int val6 = image.data[offset + (pixel_index - 1)];
	int val7 = image.data[offset + pixel_index];
	int val8 = image.data[offset + (pixel_index + 1)];
	//cout << "--------------------------------------------------------------------------" << endl;
	//cout << "["<<(pixel_index - 1) - offset << "," << pixel_index - offset << "," << pixel_index + 1 - offset << "," << (pixel_index - 1) << "," << pixel_index << "," << pixel_index + 1 << "," << offset + (pixel_index - 1) << "," << offset + pixel_index << "," << offset + pixel_index + 1 << "]" << endl;
	//cout << "[" << val0 << "," << val1 << "," << val2 << "," << val3 << "," << val4 << "," << val5 << "," << val6 << "," << val7 << "," << val8 << "]" << endl;
	//cout << "--------------------------------------------------------------------------" << endl;
	
	//cout << "filter" << "[" << filter[0] << "," << filter[1] << "," << filter[2] << "," << filter[3] << "," << filter[4] << "," << filter[5] << "," << filter[6] << "," << filter[7] << "," << filter[8] << "]" << endl;
	
	int sum = 0;
	int sum2 = 0;

	sum = val0*filter[0] + val1*filter[1] + val2*filter[2] + val3*filter[3] + val4*filter[4] + val5*filter[5] + val6*filter[6] + val7*filter[7] + val8*filter[8];
	sum2 = val0*filter2[0] + val1*filter2[1] + val2*filter2[2] + val3*filter2[3] + val4*filter2[4] + val5*filter2[5] + val6*filter2[6] + val7*filter2[7] + val8*filter2[8];

	if (sum < 255)
		{
			sum = 255;
		}
	if (sum2 < 255)
		{
			sum2 = 255;
		}
	if (sum > 255)
		{
			sum = 0;
		}
	if (sum2 > 255)
		{
			sum2 = 0;
		}
	
	Result.data[pixel_index] = sqrt(sum*sum + sum2*sum2);
	
	}


void secuential_convolution()
	{

		Mat image = imread("gato6.jpg", 0);

		if (!image.data)
		{
			printf(" No image data \n ");
		}
		else
		{
			namedWindow("window", CV_WINDOW_AUTOSIZE);
			//imshow("window", image);
		}

		//convoluted x and y empty pictures.
		Mat Copied;
		Mat Copied2;


		//unsigned char * new_pic = (unsigned char *)malloc(sizeof(unsigned char)*image.size().width*image.size().height);
		//new_pic= image.data;
		//3*3  x filter.
		int* filter = (int*)malloc(sizeof(int) * 9);
		filter[0] = -3;
		filter[1] = -10;
		filter[2] = -3;
		filter[3] = 0;
		filter[4] = 0;
		filter[5] = 0;
		filter[6] = 3;
		filter[7] = 10;
		filter[8] = 3;

		//3*3  y filter.
		int* filter2 = (int*)malloc(sizeof(int) * 9);
		filter2[0] = -3;
		filter2[1] = 0;
		filter2[2] = 3;
		filter2[3] = 10;
		filter2[4] = 0;
		filter2[5] = 10;
		filter2[6] = -3;
		filter2[7] = 0;
		filter2[8] = 3;


		image.copyTo(Copied);
		image.copyTo(Copied2);
		

		int width = image.size().width;
		int height = image.size().height;


		for (int i = image.size().width + 1; i < (width *height) - (width - 1); i++)
		{
			if (i%width == width - 1)
			{
				i += 2;
			}
			pixels_to_filfer(image, Copied, i, filter, filter2);
			//pixels_to_filfer(image, Copied2, i, filter2);

			//new_pic[i] = image.data[i];
			//cout << i << endl;
		}

		//Copied.data = new_pic;

		imshow(" my failed secuential sobel =)", Copied);
		//imshow("sobel y", Copied2);
		waitKey(1);

	}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

cl_device_id* printDeviceInfo()
{
	int i, j;
	char* value;
	size_t valueSize;
	cl_uint platformCount;
	cl_platform_id* platforms;
	cl_uint deviceCount;
	cl_device_id* devices;
	cl_uint maxComputeUnits;
	cl_ulong MEM_SIZE;

	// get all platforms
	clGetPlatformIDs(0, NULL, &platformCount);
	platforms = (cl_platform_id*)malloc(sizeof(cl_platform_id) * platformCount);
	clGetPlatformIDs(platformCount, platforms, NULL);

	for (i = 0; i < platformCount; i++) {

		// get all devices
		clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 0, NULL, &deviceCount);
		devices = (cl_device_id*)malloc(sizeof(cl_device_id) * deviceCount);
		clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, deviceCount, devices, NULL);

		// for each device print critical attributes
		for (j = 0; j < deviceCount; j++) {

			// print device name
			cout << devices[j] << endl;
			clGetDeviceInfo(devices[j], CL_DEVICE_NAME, 0, NULL, &valueSize);
			value = (char*)malloc(valueSize);
			clGetDeviceInfo(devices[j], CL_DEVICE_NAME, valueSize, value, NULL);
			printf("%d. Device: %s\n", j + 1, value);
			free(value);

			// print hardware device version
			clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, 0, NULL, &valueSize);
			value = (char*)malloc(valueSize);
			clGetDeviceInfo(devices[j], CL_DEVICE_VERSION, valueSize, value, NULL);
			printf(" %d.%d Hardware version: %s\n", j + 1, 1, value);
			free(value);

			// print software driver version
			clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, 0, NULL, &valueSize);
			value = (char*)malloc(valueSize);
			clGetDeviceInfo(devices[j], CL_DRIVER_VERSION, valueSize, value, NULL);
			printf(" %d.%d Software version: %s\n", j + 1, 2, value);
			free(value);

			// print c version supported by compiler for device
			clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, 0, NULL, &valueSize);
			value = (char*)malloc(valueSize);
			clGetDeviceInfo(devices[j], CL_DEVICE_OPENCL_C_VERSION, valueSize, value, NULL);
			printf(" %d.%d OpenCL C version: %s\n", j + 1, 3, value);
			free(value);

			// print parallel compute units
			clGetDeviceInfo(devices[j], CL_DEVICE_MAX_COMPUTE_UNITS,
				sizeof(maxComputeUnits), &maxComputeUnits, NULL);
			printf(" %d.%d Parallel compute units: %d\n", j + 1, 4, maxComputeUnits);


			/*//print max_mem_alloc_size
			clGetDeviceInfo(devices[j], CL_DEVICE_MAX_MEM_ALLOC_SIZE,
			sizeof(MEM_SIZE), &MEM_SIZE, NULL);*/

			//printf("Memory size: %d\n", MEM_SIZE);
		}
		return devices;
		free(devices);
	}

	free(platforms);

}


void showAditionalinfo(){
	cl_device_id* devices;
	char* value;
	size_t valueSize;
	cl_ulong MEM_SIZE;
	cl_ulong GLOBAL_MEM_SIZE;
	cl_ulong GLOBAL_MEM_CACHE_SIZE;
	cl_ulong LOCAL_MEM_SIZE;
	size_t *workGroupSize;

	cl_uint workGroupDimentions;
	size_t Itemsizes;
	cl_uint maxComputeUnits;


	///1.DEVASTADOR
	///2.OLAND
	///3.AMD A10-5750 APU with Radeon<tm > 

	devices = printDeviceInfo();

	int device_index = 0;


	while (1){

		cout << endl << endl << "choose number of device you want to see specs." << endl << endl;
		cin >> device_index;
		cout << "USED DEVICE." << endl;

		clGetDeviceInfo(devices[device_index], CL_DEVICE_NAME, 0, NULL, &valueSize);
		value = (char*)malloc(valueSize);
		clGetDeviceInfo(devices[device_index], CL_DEVICE_NAME, valueSize, value, NULL);
		printf("Device: %s\n", value);

		// print parallel compute units
		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_COMPUTE_UNITS,
			sizeof(maxComputeUnits), &maxComputeUnits, NULL);
		printf("Parallel compute units: %d\n", maxComputeUnits);

		//print max_mem_alloc_size
		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_MEM_ALLOC_SIZE,
			sizeof(MEM_SIZE), &MEM_SIZE, NULL);

		printf("Max Memory size: %d\n", MEM_SIZE);

		//print max_goblal_chache_alloc_size
		clGetDeviceInfo(devices[device_index], CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
			sizeof(GLOBAL_MEM_SIZE), &GLOBAL_MEM_SIZE, NULL);

		printf("Max global Memory size: %d\n", GLOBAL_MEM_SIZE);

		//print max_goblal_chacheLine_alloc_size
		clGetDeviceInfo(devices[device_index], CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
			sizeof(GLOBAL_MEM_CACHE_SIZE), &GLOBAL_MEM_CACHE_SIZE, NULL);

		printf("Max global Memory cache line  size: %d\n", GLOBAL_MEM_CACHE_SIZE);

		//prIT LOCAL MEM SIZE.
		clGetDeviceInfo(devices[device_index], CL_DEVICE_LOCAL_MEM_SIZE,
			sizeof(LOCAL_MEM_SIZE), &LOCAL_MEM_SIZE, NULL);

		printf("Max local  size: %d\n", LOCAL_MEM_SIZE);

		/*	//prIT	workgroup size.
		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_WORK_GROUP_SIZE,
		sizeof(workGroupSize), &workGroupSize, NULL);

		printf("workgroup size: %s\n", workGroupSize);*/

		/*
		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
		sizeof(workGroupDimentions), &workGroupDimentions, NULL);

		printf("workgroup dimentions: %d\n", workGroupDimentions);

		clGetDeviceInfo(devices[device_index], CL_DEVICE_MAX_WORK_ITEM_SIZES,
		sizeof(Itemsizes), &Itemsizes, NULL);

		printf("work items sizes per dimention: %s\n", Itemsizes);
		*/

		/*free(value);
		string s;
		cin >> s;*/

	}

}



cl_platform_id getPlatform()
{
	/*Step1: Getting platforms and choose an available one.*/
	cl_uint numPlatforms;	//the NO. of platforms
	cl_platform_id platform = NULL;	//the chosen platform
	cl_int	status = clGetPlatformIDs(0, NULL, &numPlatforms);
	if (status != CL_SUCCESS)
	{
		cout << "Error: Getting platforms!" << endl;
		return 0;
	}

	cout << "NUMBER OF PLATFORMS: " << endl << numPlatforms << endl;

	/*For clarity, choose the first available platform. */
	if (numPlatforms > 0)
	{
		cl_platform_id* platforms = (cl_platform_id*)malloc(numPlatforms* sizeof(cl_platform_id));
		status = clGetPlatformIDs(numPlatforms, platforms, NULL);
		platform = platforms[0];
		/*cout << "CHOOSED PLATFORM;" << endl;
		cout << platform << endl << endl;*/
		free(platforms);
	}
	return platform;
}


cl_device_id* getDevice(cl_platform_id platform)
{
	/*Step 2:Query the platform and choose the first GPU device if has one.Otherwise use the CPU as device.*/
	cl_int	status;
	cl_uint				numDevices = 0;
	cl_device_id        *devices;
	status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &numDevices);
	if (numDevices == 0)	//no GPU available.
	{
		cout << "No GPU device available." << endl;
		cout << "Choose CPU as default device." << endl;
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 0, NULL, &numDevices);
		devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, numDevices, devices, NULL);
	}
	else
	{
		devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
		status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numDevices, devices, NULL);
	}
	/*cout << "CHOOSED DEVICE." << endl;
	cout << devices << endl<<endl;*/

	return devices;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////PROGRAM STUFF///////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* convert the kernel file into a string */
int convertToString(const char *filename, std::string& s)
{
	size_t size;
	char*  str;
	std::fstream f(filename, (std::fstream::in | std::fstream::binary));

	if (f.is_open())
	{
		size_t fileSize;
		f.seekg(0, std::fstream::end);
		size = fileSize = (size_t)f.tellg();
		f.seekg(0, std::fstream::beg);
		str = new char[size + 1];
		if (!str)
		{
			f.close();
			return 0;
		}

		f.read(str, fileSize);
		f.close();
		str[size] = '\0';
		s = str;
		delete[] str;
		return 0;
	}
	cout << "Error: failed to open file\n:" << filename << endl;
	return FAILURE;
}

cl_program getCLProgram(const char *kernelName, cl_context context)
{
	cl_int	status;
	/*Step 5: Create program object */
	const char *filename = kernelName;
	string sourceStr;
	status = convertToString(filename, sourceStr);
	const char *source = sourceStr.c_str();
	size_t sourceSize[] = { strlen(source) };
	cl_program program = clCreateProgramWithSource(context, 1, &source, sourceSize, NULL);

	return program;
}



void Print_program_errors(cl_program program, cl_device_id device)
{
	cl_int status;
	size_t len;

	char *buffer;
	// check build error and build status first
	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_STATUS,
		sizeof(cl_build_status), &status, NULL);

	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &len);
	buffer = (char*)calloc(len + 1, sizeof(char));

	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, len + 1, buffer, NULL);

	cout << "ERROR BUFFER " << endl;
	cout << buffer << endl;
}

////////////PROGRAMS TO RUN  MAKE SURE YOU PASS IN THE ARGUMENTS AND SET THIS FUNTION in PROGRAM INPUT





/////////////////////////////////////////TIME RECOLECTION ////////////////////////////////////////////////////////////////////

long timediff(clock_t t1, clock_t t2) {
	long elapsed;
	elapsed = ((double)t2 - t1) / CLOCKS_PER_SEC * 1000;
	return elapsed;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CONVOLUTION(cl_context context, cl_program program, cl_command_queue& commandQueue)
{

	clock_t t1, t2;
	long elapsed;



	float promedio = 0;

	t1 = clock();

	///KEEP TRACK OF STUFF
	cl_int	status;

	//loading input data
	Mat image = imread("gato6.jpg", 0);

	if (!image.data)
	{
		printf(" No image data \n ");
	}
	else
	{
		namedWindow("input", CV_WINDOW_AUTOSIZE);
		imshow("input", image);
		waitKey(1);
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//allocating memory and initializing variables from problem in this case convolution.
	


	//image.
	unsigned char * input = (unsigned char *)malloc(sizeof(unsigned char)*image.size().width*image.size().height);

	//size of image we are loading.
	size_t strlength = image.size().width*image.size().height - 1;

	//filters.
	int* filter = (int*)malloc(sizeof(int) * 9);
	int* filter2 = (int*)malloc(sizeof(int) * 9);

	//comparison value. 
	cout << "INPUT" << endl;
	cout << (int)input << endl;

	//cout << input << endl;
	//alocating space for image data output.
	unsigned char *output = (unsigned char *)malloc(sizeof(unsigned char)*image.size().width*image.size().height);

	///setting filters and  assing input data for convolution .
	input = image.data;


	cl_int width = image.size().width;
	cl_int height = image.size().height;

	filter[0] = -1;
	filter[1] = 0;
	filter[2] = 1;
	filter[3] = -2;
	filter[4] = 0;
	filter[5] = 2;
	filter[6] = -1;
	filter[7] = 0;
	filter[8] = 1;

	//3*3  y filter.
	
	filter2[0] = -1;
	filter2[1] = -2;
	filter2[2] = -1;
	filter2[3] = 0;
	filter2[4] = 0;
	filter2[5] = 0;
	filter2[6] = 1;
	filter2[7] = 2;
	filter2[8] = 1;


	//CREATE NEEDED BUFFERS and pass in information to process.
	cl_mem inputBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (strlength + 1) * sizeof(unsigned char), (void *)input, NULL);
	cl_mem outputBuffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, (strlength + 1) * sizeof(unsigned char), NULL, NULL);
	cl_mem filter1Buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (9 + 1) * sizeof(int), (void *)filter, NULL);
	cl_mem filter2Buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (9 + 1) * sizeof(int), (void *)filter2, NULL);
	
	//NO BUFFERS NEEDED FOR INT.
	//cl_mem widthBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), (void *)width, NULL);
	//cl_mem heightBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), (void *)height, NULL);
	
	/*Step 8: Create kernel object */
	cl_kernel kernel = clCreateKernel(program, "test", NULL);

	/*Step 9: Sets Kernel arguments.*/
	status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&inputBuffer);
	status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&outputBuffer);
	status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&filter1Buffer);
	status = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&filter2Buffer);
	status = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&width);
	status = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&height);


	/*Step 10: Running the kernel. (SET DIFERENT TYPES OF INDEXING (global,local)*/

	size_t localWorkSize[3];
	size_t globalWorkSize[3];



	globalWorkSize[0] = width*height;
	globalWorkSize[1] = 0;
	globalWorkSize[2] = 0;

	localWorkSize[0] = 16;
	localWorkSize[1] = 16;
	localWorkSize[2] = 0;

	status = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);

	//status = clEnqueueNDRangeKernel(commandQueue, kernel, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);

	/*Step 11: Read the cout put back to host memory.*/
	status = clEnqueueReadBuffer(commandQueue, outputBuffer, CL_TRUE, 0, strlength * sizeof(unsigned char), output, 0, NULL, NULL);
	

	//Add the terminal character to the end of output for safety.
	output[strlength] = '\0';	


	//printing comparison value for out put compare in command screen if changed.(remember to ash why and chenck if changes result of work?).
	cout << "OUTPUT" << endl;
	cout << (int)output << endl;
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
	///creating empty image to display data after procesing. 
	Mat result;
	//copy to create an space similar to input.
	image.copyTo(result);

	//set te image to black in a akward way setting 0 in each position.  
	for (int i = 0; i < image.size().width*image.size().height; i++)
	{
		result.data[i] = 0;
	}


	//imshow("empty", result);
	
	result.data = output;

	imshow("paralel sobel.", result);
	waitKey(1);


	t2 = clock();

	elapsed = timediff(t1, t2);
	printf("elapsed: %ld ms\n\n", elapsed);

	cout << "CONVOLUTION Passed!\n";


///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//buen ciudadano y liberamos recursos en c.
	//Step 12: release resources.
	status = clReleaseKernel(kernel);//Release the program object.
	status = clReleaseMemObject(inputBuffer);		//Release mem object.
	status = clReleaseMemObject(outputBuffer);
	status = clReleaseMemObject(filter1Buffer);
	status = clReleaseMemObject(filter2Buffer);


	if (output != NULL)
	{
		free(output);
		output = NULL;
	}



}


void parallel_convolution()
	{


		//KEEP CONTROL OF OPERATION IF IT FAILS.
		cl_int	status;
		//showAditionalinfo();

		/*Step 1: Get platform.*/
		cl_platform_id platform = NULL;
		platform = getPlatform();

		/*Step 1: Get Device.*/
		cl_device_id* device;
		device = getDevice(platform);

		/*Step 3: Create context.*/
		cl_context context = clCreateContext(NULL, 1, device, NULL, NULL, NULL);

		/*Step 4: Creating command queue associate with the context.*/
		cl_command_queue commandQueue = clCreateCommandQueue(context, device[0], 0, NULL);

		/*Step 5: Get program.*/
		cl_program program;

		program = getCLProgram("kernel2.cl", context);


		/*Step 6: Build program. */
		clBuildProgram(program, 1, device, NULL, NULL, NULL);

		


		/*Step 7: INITIALIZE PROBLEM.*/
		CONVOLUTION(context, program, commandQueue);

		Print_program_errors(program, device[0]);

		/*Step 12: Clean the resources.*/
		//Release kernel.
		status = clReleaseProgram(program);
		status = clReleaseCommandQueue(commandQueue);	//Release  Command queue.
		status = clReleaseContext(context);

		if (device != NULL)
		{
			free(device);
			device = NULL;
		}

	}






int main()
	{

		clock_t t1, t2;
		long elapsed;

		float promedio = 0;

		
		for (int i = 0; i < 20; i++)
		{
			t1 = clock();

			////SECUENTIAL CODE.
			secuential_convolution();

			t2 = clock();

			elapsed = timediff(t1, t2);
			promedio += elapsed;
			printf("%i. elapsed: %ld ms\n\n", i, elapsed);
		}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//parallel_convolution();
		for (int i = 0; i <20; i++)
		{
			//t1 = clock();

			////PARALEL CODE.
			//showAditionalinfo();
			cout << i ;
			parallel_convolution();

			/*t2 = clock();

			elapsed = timediff(t1, t2);
			promedio += elapsed;
			printf("%i. elapsed: %ld ms\n\n",i, elapsed);*/
		}
		

		//printf("average time: %f\n", promedio/20);
	
		
		waitKey(0);
			

		return 0;
	}