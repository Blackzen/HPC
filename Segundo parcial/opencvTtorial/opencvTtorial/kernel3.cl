#ifdef cl_khr_fp64
    #pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
    #pragma OPENCL EXTENSION cl_amd_fp64 : enable
#else
    #error "Double precision floating point not supported by OpenCL implementation."
#endif


__kernel void test(__global unsigned char* in, __global unsigned char* out, __global int* x_filter, __global int* y_filter, __private const int width, __private const int height)
{
	int i = get_global_id(0);

	__local int filter[16];
	__local int filter2[16];


	int ite;
	for(ite =0; ite< 9 ; ite++ )
		{
					filter[ite] = x_filter[ite];
					filter2[ite] = y_filter[ite];

		}

	
	//needed if indexing al the filter treads.
	int j_x = get_local_id(0);
	int j_y = get_local_id(1);

	//printf("x: %d  ",j_x);
	//printf("y: %d  \n",j_x);


	__local int tile[16][16];




	//DATOS CARGADOS DE GLOBAL A LOCAL.
	tile[j_x][j_y]=in[i+width];
	
	
	//barrier(CLK_LOCAL_MEM_FENCE);



	float sum = 0.0;
	float sum2 = 0.0;


	if(i>width-1 && i<(width*height) -width)
		{

			/*__local int p1[j_x][j_y]= tile[j_x-1][j_y-1];
			__local int p2[16][16]= tile[j_x][j_y-1]
			__local int p3[16][16]= tile[j_x+1][j_y-1]
			__local int p4[16][16]= tile[j_x-1][j_y];
			__local int p5[16][16]= tile[j_x][j_y];
			__local int p6[16][16]= tile[j_x+1][j_y];
			__local int p7[16][16]= tile[j_x-1][j_y+1];
			__local int p8[16][16]= tile[j_x][j_y+1];
			__local int p9[16][16]= tile[j_x+1][j_y+1];*/


			if(i%width != 0 && i%width != width-1 )
				{

					/*
						//sobel x 
						sum += tile[j_x-1][j_y-1]*filter[0];
						sum += tile[j_x][j_y-1]*filter[1];
						sum += tile[j_x+1][j_y-1]*filter[2];
						sum += tile[j_x-1][j_y]*filter[3];
						sum += tile[j_x][j_y]*filter[4];
						sum += tile[j_x+1][j_y]*filter[5];
						sum += tile[j_x-1][j_y+1]*filter[6];
						sum += tile[j_x][j_y+1]*filter[7];
						sum += tile[j_x+1][j_y+1]*filter[8];

						//out[i] = sum;
						
						//sobely
						sum2 += tile[j_x-1][j_y-1]*filter2[0];
						sum2 += tile[j_x][j_y-1]*filter2[1];
						sum2 += tile[j_x+1][j_y-1]*filter2[2];
						sum2 += tile[j_x-1][j_y]*filter2[3];
						sum2 += tile[j_x][j_y]*filter2[4];
						sum2 += tile[j_x+1][j_y]*filter2[5];
						sum2 += tile[j_x-1][j_y+1]*filter2[6];
						sum2 += tile[j_x][j_y+1]*filter2[7];
						sum2 += tile[j_x+1][j_y+1]*filter2[8];
						*/
						//out[i] = sum2;

					

					//sobel 2d
						barrier(CLK_LOCAL_MEM_FENCE);
					
					
					//out[i] = sqrt(sum*sum + sum2*sum2);
					
					//out[i] = out[i];

					////DATOS CARGADOS DESDE EMROIA LOCAL A GLOBAL
					out[i] = tile[j_x][j_y];
					
					
					
					
					//
				}
			

			
		}
	
	

}
